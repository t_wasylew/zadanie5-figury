import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Write a command in this format: " + "count area/perimeter circle/square/rectangle x/y or type quit to end program");
            String command = scanner.nextLine();

            if (command.equals("")) {
                System.out.println("You typed nothing...");
                continue;
            }

            command = command.toLowerCase().trim();
            if (command.contains("quit")) {
                System.out.println("Bye!");
                break;
            }
            String[] words = command.split(" ");

            try {

                String countWord = words[0];
                String calculation = words[1];
                String figure = words[2];

                if (!countWord.equals("count")) {
                    System.out.println("Wrong command!");
                    continue;
                }

                boolean isArea = calculation.equals("area");
                boolean isPerimeter = calculation.equals("perimeter");

                switch (figure) {
                    case "square":
                        handleSquare(words, isArea, isPerimeter);
                        break;
                    case "rectangle":
                        handleRectangle(words, isArea, isPerimeter);
                        break;
                    case "circle":
                        handleCircle(words, isArea, isPerimeter);
                        break;
                    default:
                        break;
                }
            } catch (ArrayIndexOutOfBoundsException aio) {
                System.out.println("Wrong command!");
            }

        } while (scanner.hasNextLine());
    }

    private static void handleCircle(String[] words, boolean isArea, boolean isPerimeter) {
        String radius = words[3];
        double radiusNumber = Integer.parseInt(radius);
        Circle circle = new Circle(radiusNumber);

        if (isArea) {
            System.out.println("Circle area= " + circle.calculateArea());
        } else if (isPerimeter) {
            System.out.println("Circle perimeter= " + circle.calculatePerimeter());
        } else {
            System.out.println("Error! Something bad happened!");
        }
    }

    private static void handleRectangle(String[] words, boolean isArea, boolean isPerimeter) {
        String side1 = words[3];
        String side2 = words[4];
        int side1Lenght = Integer.parseInt(side1);
        int side2Lenght = Integer.parseInt(side2);

        Rectangle rectangle = new Rectangle(side1Lenght, side2Lenght);

        if (isArea) {
            System.out.println("Rectangle area= " + rectangle.calculateArea());
        } else if (isPerimeter) {
            System.out.println("Rectangle perimeter= " + rectangle.calculatePerimeter());
        } else {
            System.out.println("Error! Something bad happened!");
        }
    }


    private static void handleSquare(String[] words, boolean isArea, boolean isPerimeter) {
        String side = words[3];
        int sideLenght = Integer.parseInt(side);
        Square square = new Square(sideLenght);

        if (isArea) {
            System.out.println("Square area= " + square.calculateArea());
        } else if (isPerimeter) {
            System.out.println("Square perimeter= " + square.calculatePerimeter());
        } else {
            System.out.println("Error! Something bad happened!");
        }
    }
}
