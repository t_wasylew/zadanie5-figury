public class Rectangle {
    private double a, b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double calculatePerimeter() {
        return (2 * a) + (2 * b);
    }

    public double calculateArea() {
        return a * b;
    }
}
